---
title: "Intro to exuber"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Intro to exuber}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  message=FALSE
)
```

```{r echo=FALSE, message=FALSE}
library(exuber)
```

For our analysis we are going to use the `datasets::EuStockMarkets` dataset, which contains the daily closing prices of four major European stock indices: Germany DAX (Ibis), Switzerland SMI, France CAC, and UK FTSE(see `?EuStockMarkets`). The data are sampled in business time, i.e., weekends and holidays are omitted. In this particular exercise we want to focus on weekly observations. To do so we aggregate to a weekly frequency and reduce the number of observations from 1860 to 372. 

```{r dataset}
stocks <- aggregate(EuStockMarkets, nfrequency = 52, mean)
```

## Estimation

We estimate the above series using the recursive Augmented Dickey-Fuller test with 1 lag.

```{r estimation}
est_stocks <- radf(stocks,lag = 1)
```

## Report

The summary will print the t-stat and the critical values for 90, 95 and 99 significance level. The package provides simulated critical values, so we use them by not specifying the `cv` argument into the `summary` function. 

```{r summary}
summary(est_stocks)
```

It seems that all stocks exhibit exuberant behaviour but we can also use a `diagnostics()` to do it for us. This is extremely useful when we deal a lot of series.

```{r diagnostics}
diagnostics(est_stocks)
```

The `autoplot.radf` function returns a list of ggplot2, thus we need to grob the list into a
single graph. Function `ggarrange` does this for us, while we can specify the arrangement of the graphs by supplying nrow and ncol arguments.

```{r datestamp, fig.width = 9}
autoplot(est_stocks)
```

If we need to know the exact period of exuberance we can do so with the function `datestamp()`. `datestamp()` works in a similar manner, where the user has to specify the critical values, however we can still utilize the package's critical values by leaving the cv-argument blank.

```{r}
# Minimum duration of an explosive period 
rot = round(log(nrow(stocks))) # log(n) ~ rule of thumb

datestamp(est_stocks, min_duration = rot)
```

Finally, we can plot just the periods the periods of exuberance. Plotting datestamp object is particularly useful when we have a lot  of series, and we are interested to identify explosive patterns.

```{r fig.width = 7, fig.height=2}
datestamp(est_stocks) %>% 
  autoplot()
```
